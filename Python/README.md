# Diferentes formas de Hacer "¡Un Hola Mundo!" En Python
Aquí les estare dejando capturas del output de los archivos.
#### [La tipica forma básica](./Basico.py):

![Forma Basica](./../src/Python/Basico.png)

#### [Usando Funciones](./ConFuncion.py):

![Forma con Funciones](./../src/Python/Con_Funcion.png)

#### [Usando Clases](./ConClases.py):

![Forma con Clases](./../src/Python/Con_Clases.png)

#### [Usando Funciones y Clases](./Con_Clases_y_Funciones.py):

![Forma con Clases y Funciones](./../src/Python/Con_Clases_y_Funciones.png)
